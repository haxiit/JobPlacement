CREATE TABLE IF NOT EXISTS employees (
    id UUID PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    birth_date TIMESTAMP,
    serial_id_number TEXT,
    pesel TEXT UNIQUE,
    email TEXT UNIQUE,
    contact_number TEXT,
    cv_uri TEXT,
    street TEXT,
    postal_code TEXT,
    city TEXT
);

CREATE TABLE IF NOT EXISTS skills (
    id UUID PRIMARY KEY NOT NULL,
    name TEXT UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS employee_skills (
    employee_id UUID NOT NULL,
    skill_id UUID NOT NULL,
    PRIMARY KEY (employee_id, skill_id),
    CONSTRAINT employee_fk FOREIGN KEY (employee_id) REFERENCES employees (id),
    CONSTRAINT skill_fk FOREIGN KEY (skill_id) REFERENCES skills(id)
);