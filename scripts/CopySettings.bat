@echo off

REM This script copies settings file to a relevant build folder (Debug/Release)
REM It takes three arguments 
REM 1 - Solution Dir Absolute Path 
REM 2 - Target Dir Absolute Path
REM 3 - File to be copied name (i.e. AppSettings.json)

IF NOT EXIST %~1%~3 EXIT 1
IF NOT EXIST %~2 EXIT 1
IF [%3] == [] EXIT 1

COPY %~1%~3 %~2%~3 


