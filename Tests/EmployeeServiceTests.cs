﻿/*
using System;
using System.Threading.Tasks;
using AutoMapper;
using Core.Employees.Entities;
using Core.Employees.Repositories;
using Infrastructure.DTO.Incoming;
using Infrastructure.Services.Employees;
using Moq;
using NUnit.Framework;

namespace Tests
{
    public class EmployeeServiceTests
    {
        [Test]
        public async Task add_user_should_invoke_add_on_employee_repository()
        {
            var employeeRepository = new Mock<IEmployeeRepository>();
            var mapper = new Mock<IMapper>();
            var employeeService = new EmployeeService(employeeRepository.Object, mapper.Object);
            var employeeDescriptor = new EmployeeDescriptor
            {
                Address = new AddressDescriptor()
                {
                    City = "Szczecin",
                    PostalCode = "81-231",
                    Street = "Zurawskiego"
                },
                Birthdate = DateTime.Now,
                ContactNumber = "9842930123",
                Email = "gunwo@buziaczek.pl",
                LastName = "ok",
                Name = "nope",
                Pesel = "940234123123",
                SerialIdNumber = "AYT9213"
            };

            await employeeService.Add(employeeDescriptor);

            employeeRepository.Verify(x => x.Add(It.IsAny<Employee>()), Times.Once());
        }
    }
}
*/
