﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Employees.Entities;

namespace Core.Employees.Repositories
{
    public interface IEmployeeRepository : IRepository
    {
        Task Add(Employee employee);
        Task Update(Employee employee);
        Task<Employee> Get(Guid id);
        Task<Employee> GetByEmail(string email);
        Task<List<Employee>> Browse();
        Task Delete(Guid id);
        Task DeleteByEmail(string email);
    }
}
