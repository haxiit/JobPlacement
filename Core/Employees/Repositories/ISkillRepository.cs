﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Employees.Entities;

namespace Core.Employees.Repositories
{
    public interface ISkillRepository : IRepository
    {
        Task Add(Skill skill);
        Task AddMany(IEnumerable<Skill> skills);
        Task<Skill> Get(Guid skillId);
        Task<Skill> GetByName(string skillName);
        Task<IEnumerable<Skill>> Browse();
        Task Update(Skill skill);
        Task Delete(Guid skillId);
    }
}
