﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using Core.Employees.Exceptions;
using Core.Extensions;
using Core.Shared;
using Core.Shared.Exceptions;
using static System.String;

namespace Core.Employees.Entities
{
    public class Employee
    {
        private IEnumerable<Skill> skills;

        public Guid Id { get; protected set; }
        public string Name { get; protected set; }
        public string LastName { get; protected set; }
        public DateTime BirthDate { get; protected set; }
        public string SerialIdNumber { get; protected set; }
        public string Pesel { get; protected set; }
        public string Email { get; protected set; }
        public string ContactNumber { get; protected set; }
        public Address Address { get; protected set; }
        public string CvUri { get; protected set; }
        public Guid CompanyId { get; protected set; }
        public Guid ContractId { get; protected set; }

        public IEnumerable<Skill> Skills
        {
            get => EmployeeSkills != null ? EmployeeSkills.Select(employeeSkill => employeeSkill.Skill).ToList() :
                                            new List<Skill>();
            protected set
            {
                EmployeeSkills = value.Select(skill => new EmployeeSkill
                {
                    Skill = skill,
                    SkillId = skill.Id,
                    Employee = this,
                    EmployeeId = Id
                })
                .ToList();
            }
        }

        public ICollection<EmployeeSkill> EmployeeSkills { get; protected set; }

        protected Employee() { }

        public static Employee Create(string name, string lastName, DateTime birthdate, string serialIdNumber,
            string pesel, string email, string contactNumber, Address address, IEnumerable<Skill> skills, string cvUri = null)
        {
            var employee =  new Employee()
            {
                Id = Guid.NewGuid(),
                Name = name,
                LastName = lastName,
                BirthDate = birthdate,
                SerialIdNumber = serialIdNumber,
                Pesel = pesel,
                Email = email,
                ContactNumber = contactNumber,
                Address = address,
                CvUri = cvUri,
                EmployeeSkills = new List<EmployeeSkill>(),
                Skills = skills
            };

            if (IsNullOrWhiteSpace(employee.Email) && IsNullOrWhiteSpace(employee.ContactNumber))
            {
                throw new PhoneOrEmailRequiredException("Phone or email is required");
            }

            return employee;
        }

        public void ChangeName(string name)
        {
            if (!name.Empty())
            {
                Name = name;
            }
        }

        public void ChangeLastName(string lastName)
        {
            if (!lastName.Empty())
            {
                LastName = lastName;
            }
        }
        public void ChangeSerialIdNumber(string serialIdNumber)
        {
            if (!serialIdNumber.Empty())
            {
                SerialIdNumber = serialIdNumber;
            }
        }

        public void ChangePesel(string pesel)
        {
            if (!pesel.Empty())
            {
                Pesel = pesel;
            }
        }

        public void ChangeEmail(string email)
        {
            try
            {
                var mailAddress = new MailAddress(email);
                Email = mailAddress.Address.ToLowerInvariant();
            }
            catch
            {
                throw new InvalidEmailException($"Given email {email} is not valid");
            }
        }

        public void ChangeNumber(string number)
        {
            if (!number.Empty())
            {
                ContactNumber = number;
            }
        }

        public void ChangeAddress(Address address)
        {
            Address = address;
        }

        public void ChangeSkills(IEnumerable<Skill> skills)
        {
            Skills = skills.ToList();
        }
        public void AssignContract(Guid contractId)
        {
            ContractId = contractId;
        }

        public void DiscardContract(Guid contractId)
        {
            ContractId = Guid.Empty;
        }

        public void Hire(Guid companyId)
        {
            CompanyId = companyId;
        }

        public void Fire(Guid companyId)
        {
            CompanyId = Guid.Empty;
        }

        public void AttachCv(string cvUri)
        {
            if (cvUri.Empty() || !Uri.IsWellFormedUriString(cvUri, UriKind.Absolute)) 
                throw new InvalidCvUriException($"Given {cvUri} CV path is not valid");

            CvUri = cvUri;
        }
    }
}
