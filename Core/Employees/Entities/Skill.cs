﻿using System;
using System.Collections.Generic;

namespace Core.Employees.Entities
{
    public class Skill
    {
       public Guid Id { get; protected set; }
       public string Name { get; protected set; }
        public ICollection<EmployeeSkill> EmployeeSkills { get; protected set; }

        protected Skill() {}

        public Skill(string name)
        {
            Id = Guid.NewGuid();
            Name = name;
            EmployeeSkills = new List<EmployeeSkill>();
        }

        public override bool Equals(object obj)
        {
            return obj is Skill skill && skill.Name.Equals(Name);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Id.GetHashCode() * 397) ^ (Name != null ? Name.GetHashCode() : 0);
            }
        }
    }
}
