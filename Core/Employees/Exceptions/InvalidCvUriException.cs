﻿namespace Core.Employees.Exceptions
{
    public class InvalidCvUriException : DomainException
    {
        public InvalidCvUriException(string message) : base(message)
        {
        }
    }
}
