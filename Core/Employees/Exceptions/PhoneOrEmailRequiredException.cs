﻿namespace Core.Employees.Exceptions
{
    public class PhoneOrEmailRequiredException : DomainException
    {
        public PhoneOrEmailRequiredException(string message) : base(message)
        {
        }
    }
}
