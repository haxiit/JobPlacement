﻿namespace Core.Extensions
{
    public static class StringExtensions
    {
        public static bool Empty(this string val)
        {
            return string.IsNullOrWhiteSpace(val);
        }
    }
}
