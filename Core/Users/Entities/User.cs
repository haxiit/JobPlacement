﻿using System;

namespace Core.Users.Entities
{
    public class User
    {
        public Guid Id { get; protected set; }
        public string Email { get; protected set; }
        public string Name { get; protected set; }
        public string LastName { get; protected set; }
        public string Password { get; protected set; }
        public string Salt { get; protected set; }
        public string Role { get; protected set; }

        protected User()
        {
        }

        public static User Create(string email, string name, string lastName, string password, string salt, string role = "Normal")
        {
            return new User()
            {
                Id = Guid.NewGuid(),
                Email = email,
                Name = name,
                LastName = lastName,
                Password = password,
                Salt = salt,
                Role = role
            };
        }
    }
}
