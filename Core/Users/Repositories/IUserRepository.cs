﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Users.Entities;

namespace Core.Users.Repositories
{
    public interface IUserRepository : IRepository
    {
        Task Add(User user);
        Task Update(User user);
        Task<User> Get(Guid id);
        Task<User> GetByEmail(string email);
        Task<IEnumerable<User>> Browse();
        Task Delete(Guid id);
    }
}
