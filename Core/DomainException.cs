﻿using System;

namespace Core
{
    public abstract class DomainException : Exception
    {
        public DomainException(string message) : base(message)
        {
        }
    }
}
