﻿using System;

namespace Core.Shared
{
    public class Address
    {
        public string Street { get; private set; }
        public string PostalCode { get; private set; }
        public string City { get; private set; }

        protected Address()
        {
        }

        public Address(string street, string postalCode, string city)
        {
            Street = street;
            PostalCode = postalCode;
            City = city;
        }
    }
}
