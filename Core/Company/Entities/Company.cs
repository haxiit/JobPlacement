﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using Core.Extensions;
using Core.Shared;
using Core.Shared.Exceptions;

namespace Core.Company.Entities
{
    public class Company
    {
        public Guid Id { get; private set; }
        public string Name { get; private set; }
        public ISet<Address> Locations { get; private set; }
        /// <summary>
        /// Example: NIP
        /// </summary>
        public string Identifier { get; private set; } 
        public string ContactNumber { get; private set; }  //to be discussed where and how it should be stored
        public string Email { get; private set; }

        protected Company()
        {
        }

        public Company(string name, ISet<Address> locations, string identifier, string contactNumber, string email)
        {
            Id = Guid.NewGuid();
            Name = name;
            Locations = locations;
            Identifier = identifier;
            ContactNumber = contactNumber;
            Email = email;
        }

        public void AddBranch(Address address)
        {
            Locations.Add(address);
        }

        public void RemoveBranch(Address address)
        {
            Locations.Remove(address);
        }

        public void ChangeName(string name)
        {
            Name = name;
        }

        public void ChangeIdentifier(string identifier)
        {
            Identifier = identifier;
        }

        public void ChangeContactNumber(string contactNumber)
        {
            if (!contactNumber.Empty())
            {
                ContactNumber = contactNumber;
            }
        }

        public void ChangeEmail(string email)
        {
            try
            {
                var mailAddress = new MailAddress(email);
                Email = mailAddress.Address.ToLowerInvariant();
            }
            catch
            {
                throw new InvalidEmailException($"Given email {email} is not valid");
            }
        }
    }
}
