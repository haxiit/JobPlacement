﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Company.Repositories
{
    public interface ICompanyRepository : IRepository
    {
        Task Add(Entities.Company company);
        Task Update(Entities.Company company);
        Task<Entities.Company> Get(Guid id);
        Task<List<Entities.Company>> Browse();
        Task Delete(Guid id);
    }
}
