﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Contract.Repositories
{
    public interface IContractRepository : IRepository

    {
    Task Add(Entities.Contract Contract);
    Task Update(Entities.Contract Contract);
    Task<Entities.Contract> Get(Guid id);
    Task<List<Entities.Contract>> Browse();
    Task Delete(Guid id);
    }
}
