﻿using Core.Shared;
using System;

namespace Core.Contract.Entities
{
    public class Contract
    {
        public Guid Id { get; private set; }
        public DateTime StartDate { get; private set; }
        public DateTime DueDate { get; private set; }
        public Address Location { get; private set; }
        public string EmployeePosition { get; private set; }

        protected Contract()
        {
        }


        public Contract(DateTime startDate, DateTime dueDate, Address location)
        {
            Id = Guid.NewGuid();
            StartDate = startDate;
            DueDate = dueDate;
            Location = location;
        }
    }
}
