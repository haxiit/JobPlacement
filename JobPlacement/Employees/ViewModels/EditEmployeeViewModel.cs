﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Core.Employees.Entities;
using Core.Employees.Repositories;
using Infrastructure.DTO.Incoming;
using Infrastructure.DTO.Outgoing;
using Infrastructure.Events.Employee;
using Infrastructure.Handlers;
using Infrastructure.Services.Employees;
using Xceed.Wpf.Toolkit;

namespace JobPlacement.Employees.ViewModels
{
    public class EditEmployeeViewModel : ViewModel
    {
        private readonly IEmployeeService employeeService;
        private readonly IEventBus eventBus;
        private readonly ISkillRepository skillRepository;

        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public DateTime Birthdate { get; set; }
        public string SerialIdNumber { get; set; }
        public string Pesel { get; set; }
        public string ContactNumber { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string CvPath { get; set; }
        public ObservableCollection<Skill> Skills { get; set; }
        public ObservableCollection<Skill> SelectedSkills { get; set; }
        public CheckListBox CheckListBox { get; set; }

        public EditEmployeeViewModel(IEmployeeService employeeService, IEventBus eventBus, ISkillRepository skillRepository)
        {
            this.employeeService = employeeService;
            this.eventBus = eventBus;
            this.skillRepository = skillRepository;
            InitializeAsync();
        }

        private async void InitializeAsync()
        {
            Skills = new ObservableCollection<Skill>(await skillRepository.Browse());
        }

        private ICommand updateCommand;
       
        public ICommand UpdateCommand => updateCommand ?? (updateCommand = new RelayCommand(async () =>
        {
            var address = new AddressDescriptor
            {
                City = City,
                PostalCode = PostalCode,
                Street = Street
            };

            var employee = new EmployeeDescriptor
            {
                Name = Name,
                Address = address,
                Birthdate = Birthdate,
                ContactNumber = ContactNumber,
                Email = Email,
                LastName = Lastname,
                Pesel = Pesel,
                SerialIdNumber = SerialIdNumber,
                CvUri = CvPath,
                Skills = SelectedSkills
            };

            await employeeService.Update(employee);

            await eventBus.Dispatch(new EmployeeUpdated {Employee = await employeeService.GetByEmail(employee.Email)});
        }));

        public void LoadEmployeeDescriptor(EmployeeDescriptor employee)
        {
            Name = employee.Name;
            City = employee.Address.City;
            Street = employee.Address.Street;
            PostalCode = employee.Address.PostalCode;
            Birthdate = employee.Birthdate;
            ContactNumber = employee.ContactNumber;
            Email = employee.Email;
            Lastname = employee.LastName;
            Pesel = employee.Pesel;
            SerialIdNumber = employee.SerialIdNumber;
            CvPath = employee.CvUri;
            SelectedSkills = new ObservableCollection<Skill>(employee.Skills);
        }
    }
}
