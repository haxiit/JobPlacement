﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using Infrastructure.DTO.Outgoing;
using Infrastructure.Events.Employee;
using Infrastructure.Handlers;
using Infrastructure.Services.Employees;
using JobPlacement.Employees.Views;

namespace JobPlacement.Employees.ViewModels
{
    public class EmployeeBrowserViewModel : ViewModel, IEventHandler<EmployeeDeleted>, IEventHandler<EmployeeAdded>, IEventHandler<EmployeeFilterChanged>, IEventHandler<EmployeeUpdated>
    {
        private readonly IEmployeeService employeeService;
        private readonly IQueryBus queryBus;
        private readonly IEventBus eventBus;
        private EmployeeDto selectedEmployee;
        private ICommand deleteCommand;
        private ICommand updateCommand;
        
        public EmployeeDto SelectedEmployee
        {
            get => selectedEmployee;
            set
            {
                selectedEmployee = value;
                NotifyPropertyChanged(nameof(SelectedEmployee));
                NotifySelectionChanged();
            }
        }
    
        public ObservableCollection<EmployeeDto> EmployeesList { get; set; }

        public EmployeeBrowserViewModel(IEmployeeService employeeService, IQueryBus queryBus, IEventBus eventBus)
        {
            this.employeeService = employeeService;
            this.eventBus = eventBus;
            InitializeAsync();
        }

        public ICommand DeleteCommand =>
            deleteCommand ?? (deleteCommand = new RelayCommand(async () =>
            {
                if (MessageBox.Show("Czy na pewno chcesz usunąć pracownika?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    await employeeService.DeleteByEmail(SelectedEmployee.Email);
                }
            }));

        public ICommand UpdateCommand =>
            updateCommand ?? (updateCommand = new RelayCommand(async () =>
            {
                var employee = await employeeService.GetDescriptiorByEmail(SelectedEmployee.Email);
                var employeeWindow = new EditEmployee();
                ((EditEmployeeViewModel)employeeWindow.DataContext).LoadEmployeeDescriptor(employee);
                employeeWindow.Show();
            }));

        private async void InitializeAsync()
        {
           EmployeesList = new ObservableCollection<EmployeeDto>(await employeeService.Browse());
        }

        private async void NotifySelectionChanged()
        {
            await eventBus.Dispatch(new EmployeeSelectionChanged { Employee = selectedEmployee });
        }

        public async Task Handle(EmployeeDeleted @event)
        {
            var employee = EmployeesList.SingleOrDefault(e => e.Email.Contains(@event.Email));
            if (employee != null)
            {
                EmployeesList.Remove(employee);
            }

            await Task.CompletedTask;
        }

        public async Task Handle(EmployeeAdded @event)
        {
            EmployeesList.Add(@event.Employee);
            await Task.CompletedTask;
        }

        public async Task Handle(EmployeeFilterChanged @event)
        {
            EmployeesList = new ObservableCollection<EmployeeDto>(@event.FilteredEmployees);
            await Task.CompletedTask;
        }

        public async Task Handle(EmployeeUpdated @event)
        {
            var employeeToUpdate = EmployeesList.SingleOrDefault(employee => employee.Email.Equals(@event.Employee.Email));
            var index = EmployeesList.IndexOf(employeeToUpdate);
            EmployeesList[index] = @event.Employee;
            await Task.CompletedTask;
        }
    }
}
