﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using Core.Employees.Entities;
using Core.Employees.Repositories;
using Infrastructure.DTO.Incoming;
using Infrastructure.DTO.Outgoing;
using Infrastructure.Services.Employees;
using Microsoft.Win32;

namespace JobPlacement.Employees.ViewModels
{
    class AddEmployeeViewModel : ViewModel
    {
        private readonly IEmployeeService employeeService;
        private readonly ISkillService skillService;
        private readonly ISkillRepository skillRepository;
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public DateTime Birthdate { get; set; }
        public string SerialIdNumber { get; set; }
        public string Pesel { get; set; }
        public string ContactNumber { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string CvPath { get; set; }

        public ObservableCollection<Skill> Skills { get; set; }
        public ObservableCollection<Skill> SelectedSkills { get; set; }
       

        public AddEmployeeViewModel(IEmployeeService employeeService, ISkillService skillService, ISkillRepository skillRepository)
        {
            this.employeeService = employeeService;
            this.skillService = skillService;
            this.skillRepository = skillRepository;
            SelectedSkills = new ObservableCollection<Skill>();
            InitializeAsync();
        }

        private async void InitializeAsync()
        {
            Skills = new ObservableCollection<Skill>(await skillRepository.Browse());
        }

        #region Commands
        
        private ICommand addEmployee;
        private ICommand browseCv;
        private ICommand selectSkillCommand;

        public ICommand SelectSkillCommand => selectSkillCommand ?? (selectSkillCommand = new RelayParameterizedCommand((o) =>
        {
            var skill = o as Skill;

            if (SelectedSkills.Contains(skill))
            {
                SelectedSkills.Remove(skill);
                return;
            }
            SelectedSkills.Add(skill);
        }));
        public ICommand BrowseCvCommand => browseCv ?? (browseCv = new RelayCommand(async () =>
        {
            var fileBrowser = new OpenFileDialog
            {
                Filter = "Pdf files (.pdf)|*.pdf",
                CheckFileExists = true
            };

            if (fileBrowser.ShowDialog().GetValueOrDefault(false))
            {
                var path = fileBrowser.FileName;
                
                if (string.IsNullOrWhiteSpace(path) || Uri.IsWellFormedUriString(path, UriKind.RelativeOrAbsolute))
                    return;

                CvPath = path;
            }
        }));

        public ICommand AddEmployeeCommand => addEmployee ?? (addEmployee = new RelayCommand(async () =>
        {
            var address = new AddressDescriptor
            {
                City = City,
                PostalCode = PostalCode,
                Street = Street
            };

            var employee = new EmployeeDescriptor
            {
                Name = Name,
                Address = address,
                Birthdate = Birthdate,
                ContactNumber = ContactNumber,
                Email = Email,
                LastName = Lastname,
                Pesel = Pesel,
                SerialIdNumber = SerialIdNumber,
                CvUri = CvPath ?? "",
		        Skills = SelectedSkills
            };

            await employeeService.Add(employee);

            if (MessageBox.Show("Dodano pracownika", "", MessageBoxButton.OK) != MessageBoxResult.None)
            {
                ResetValues();
            }
        }));

        #endregion

        private void ResetValues()
        {
            Name = "";
            Lastname = "";
            Birthdate = DateTime.Now;
            Email = "";
            SerialIdNumber = "";
            Pesel = "";
            ContactNumber = "";
            Street = "";
            City = "";
            PostalCode = "";
            SelectedSkills = new ObservableCollection<Skill>();

        }
    }
}
