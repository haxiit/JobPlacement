﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Core.Employees.Repositories;
using Infrastructure.DTO.Outgoing;
using Infrastructure.Services.Employees;

namespace JobPlacement.Employees.ViewModels
{
    public class EditSkillsViewModel : ViewModel
    {
        private ISkillService skillService;
        public ObservableCollection<SkillDto> Skills { get; set; }
        public SkillDto SelectedSkill { get; set; }
        public string SkillToAdd { get; set; }

        private ICommand addSkillCommand;
        private ICommand removeSkillCommand;

        public ICommand AddSkillCommand => addSkillCommand ?? (addSkillCommand = new RelayCommand(async () =>
        {
            if (!string.IsNullOrWhiteSpace(SkillToAdd))
            {
                Skills.Add(new SkillDto(){Name = SkillToAdd });
                await skillService.Add(new SkillDto {Name = SkillToAdd});
                Skills = new ObservableCollection<SkillDto>(Skills.OrderBy(skill => skill.Name));
                SkillToAdd = string.Empty;
            }
        }));
        public ICommand RemoveSkillCommand => removeSkillCommand ?? (removeSkillCommand = new RelayCommand(async () =>
        {
            if (SelectedSkill == null) return;
            await skillService.Delete(SelectedSkill);
            Skills.Remove(SelectedSkill);
        }));
        public EditSkillsViewModel(ISkillService skillService)
        {
            this.skillService = skillService;
            InitializeAsync();
        }

        private async void InitializeAsync()
        {
            Skills = new ObservableCollection<SkillDto>(await skillService.Browse());
        }
    }
}
