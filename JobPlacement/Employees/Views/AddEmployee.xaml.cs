﻿using System;
using System.Windows;
using JobPlacement.Employees.ViewModels;

namespace JobPlacement.Employees.Views
{
    /// <summary>
    /// Interaction logic for AddEmployee.xaml
    /// </summary>
    public partial class AddEmployee : Window
    {
        public AddEmployee()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            var viewModel = ViewModelResolver.Instance.ResolveViewModel<AddEmployeeViewModel>();
            DataContext = viewModel;

        }
    }
}
