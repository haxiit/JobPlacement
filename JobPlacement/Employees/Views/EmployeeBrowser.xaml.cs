﻿using System.Windows;
using System.Windows.Controls;
using JobPlacement.Employees.ViewModels;

namespace JobPlacement.Employees.Views
{
    /// <summary>
    /// Interaction logic for EmployeeBrowser.xaml
    /// </summary>
    public partial class EmployeeBrowser : Page
    {
        public EmployeeBrowser()
        {
            InitializeComponent();
            var viewModel = ViewModelResolver.Instance.ResolveViewModel<EmployeeBrowserViewModel>();
            DataContext = viewModel;
        }
    }
}
