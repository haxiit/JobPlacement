﻿using System.Windows;
using JobPlacement.Employees.ViewModels;

namespace JobPlacement.Employees.Views
{
    /// <summary>
    /// Interaction logic for EditSkills.xaml
    /// </summary>
    public partial class EditSkills : Window
    {
        public EditSkills()
        {
            InitializeComponent();
            DataContext = ViewModelResolver.Instance.ResolveViewModel<EditSkillsViewModel>();
        }
    }
}
