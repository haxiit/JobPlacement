﻿using System.Windows;
using JobPlacement.Employees.ViewModels;

namespace JobPlacement.Employees.Views
{
    /// <summary>
    /// Interaction logic for EditEmployeeViewModel.xaml
    /// </summary>
    public partial class EditEmployee : Window
    {
        public EditEmployee()
        {
            InitializeComponent();
            var viewModel = ViewModelResolver.Instance.ResolveViewModel<EditEmployeeViewModel>();
            DataContext = viewModel;
            viewModel.CheckListBox = _listBox;
        }
    }
}
