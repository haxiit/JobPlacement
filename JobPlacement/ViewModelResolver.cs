﻿using System;
using Autofac;

namespace JobPlacement
{
    public class ViewModelResolver
    {
        private static readonly Lazy<ViewModelResolver> instance = new Lazy<ViewModelResolver>(() => new ViewModelResolver());
        private IContainer container;
        private bool initialized;

        public static ViewModelResolver Instance => instance.Value;

        public void Initialize(IContainer container)
        {
            if (initialized)
            {
                throw new InvalidOperationException($"{GetType().Name} has been already initialized");
            }

            initialized = true;
            this.container = container;
        }

        public T ResolveViewModel<T>()
        {
            return container.Resolve<T>();
        }
    }
}
