﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace JobPlacement.AttachedPropeties
{
    public class BindableSourceProperty : BaseAttachedProperty<BindableSourceProperty, Uri>
    {
        public override void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (sender is WebBrowser webBrowser)
            {
                webBrowser.Source = e.NewValue as Uri;
            }
        }
    }
}
