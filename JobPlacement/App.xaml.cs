﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using Autofac;
using Infrastructure.IoC;
using Infrastructure.IoC.Modules;
using Infrastructure.Services.Utils;
using JobPlacement.Config;
using Microsoft.Extensions.Configuration;

namespace JobPlacement
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private IContainer container;

        private async void App_OnStartup(object sender, StartupEventArgs e)
        {
            AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;

            var configuration = new ConfigurationBuilder()
                .AddJsonFile("AppSettings.json", optional: false)
                .Build();

            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterModule<ContainerModule>();
            containerBuilder.RegisterModule(new SettingsModule(configuration));
            containerBuilder.RegisterModule<GuiModule>();
            container = containerBuilder.Build();

            ViewModelResolver.Instance.Initialize(container);

            var appData = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "JobPlacement");
            if (!Directory.Exists(appData))
            {
                Directory.CreateDirectory(appData);
            }

            if (Debugger.IsAttached)
            {
                var dataSeeder = container.Resolve<IDataSeeder>();
               // await dataSeeder.SeedData();
            }
        }

        private void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            // TODO: Implement something smart
            Console.Error.WriteLine(e.ExceptionObject);
        }
    }
}
