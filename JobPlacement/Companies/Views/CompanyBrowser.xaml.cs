﻿using System.Windows;
using JobPlacement.Companies.ViewModels;
using JobPlacement.Employees.ViewModels;

namespace JobPlacement.Companies.Views
{
    /// <summary>
    /// Interaction logic for CompanyBrowser.xaml
    /// </summary>
    public partial class CompanyBrowser : Window
    {
        public CompanyBrowser()
        {
            InitializeComponent();
            DataContext = ViewModelResolver.Instance.ResolveViewModel<CompanyBrowserViewModel>();
        }
    }
}
