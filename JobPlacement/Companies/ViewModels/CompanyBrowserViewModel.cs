﻿using System.Collections.ObjectModel;
using Infrastructure.DTO.Outgoing;
using Infrastructure.Services.Companies;

namespace JobPlacement.Companies.ViewModels
{
    public class CompanyBrowserViewModel : ViewModel
    {
        private readonly ICompanyService companyService;
        private ObservableCollection<CompanyDto> companiesList;

        public ObservableCollection<CompanyDto> CompaniesList { get; set; }

        public CompanyBrowserViewModel(ICompanyService companyService)
        {
            this.companyService = companyService;
            InitializeAsync();
        }

        private async void InitializeAsync()
        {
            var companies = await companyService.Browse();
            CompaniesList = new ObservableCollection<CompanyDto>(companies);
        }
    }
}
