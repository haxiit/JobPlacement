﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using Infrastructure.Events.Employee;
 using Infrastructure.Handlers;
using Infrastructure.Services.Employees;

namespace JobPlacement.UIControls.PDFViewer.ViewModels
{
    public class PdfViewerViewModel : ViewModel, IEventHandler<EmployeeSelectionChanged>
    {
        private readonly ICvStorageManager cvStorageManager;

        public Uri PdfSource { get; set; }

        public PdfViewerViewModel(ICvStorageManager cvStorageManager)
        {
            this.cvStorageManager = cvStorageManager;
            PdfSource = new Uri("about:blank");
        }

        public async Task Handle(EmployeeSelectionChanged @event)
        {
            if (@event.Employee == null || string.IsNullOrWhiteSpace(@event.Employee.CvUri))
            {
                PdfSource = new Uri("about:blank");
                return;
            }

            var fileName = Path.GetFileName(@event.Employee.CvUri);
            var localCv = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "JobPlacement", fileName);

            if (!File.Exists(localCv))
            {
                try
                {
                    await cvStorageManager.DownloadCv(@event.Employee.CvUri);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
            PdfSource = new Uri(localCv);
        }
    }
}
