using System.Windows.Controls;
using JobPlacement.UIControls.PDFViewer.ViewModels;

namespace JobPlacement.UIControls.PDFViewer.Views
{
    /// <summary>
    /// Interaction logic for PdfViewer.xaml
    /// </summary>
    public partial class PdfViewer : Page
    {
        public PdfViewer()
        {
            InitializeComponent();
            DataContext = ViewModelResolver.Instance.ResolveViewModel<PdfViewerViewModel>();
        }
    }
}
