﻿using System.Windows.Controls;
using JobPlacement.UIControls.Filter.ViewModels;

namespace JobPlacement.UIControls.Filter.Views
{
    /// <summary>
    /// Interaction logic for Filter.xaml
    /// </summary>
    public partial class Filter : Page
    {
        public Filter()
        {
            InitializeComponent();
            var viewModel = ViewModelResolver.Instance.ResolveViewModel<FilterViewModel>();
            DataContext = viewModel;
        }
    }
}
