﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Infrastructure.Configuration;
using Infrastructure.DTO.Outgoing;
using Infrastructure.Events.Employee;
using Infrastructure.Handlers;
using Infrastructure.Queries.Employees;
using Infrastructure.Services.Employees;

namespace JobPlacement.UIControls.Filter.ViewModels
{
    public class FilterViewModel : ViewModel, IEventHandler<SkillAdded>, IEventHandler<SkillDeleted>
    {
        private readonly EmployeeSettings employeeSettings;
        private readonly IQueryBus queryBus;
        private readonly IEventBus eventBus;
        private readonly ISkillService skillService;

        private string name;
        private string lastName;
        private int minAge;
        private int maxAge;
        private string contactNumber;
        private string email;
        private ObservableCollection<SkillDto> skills;
        private ObservableCollection<SkillDto> selectedSkills;

        #region Properties

        public string Name
        {
            get => name;
            set
            {
                name = value;
                NotifyPropertyChanged(nameof(Name));
                FilterData();
            }
        }

        public string LastName
        {
            get => lastName;
            set
            {
                lastName = value;
                NotifyPropertyChanged(nameof(LastName));
                FilterData();
            }
        }

        public int MinAge
        {
            get => minAge;
            set
            {
                minAge = value;
                NotifyPropertyChanged(nameof(MinAge));
                FilterData();
            }
        }

        public int MaxAge
        {
            get => maxAge;
            set
            {
                maxAge = value;
                NotifyPropertyChanged(nameof(MaxAge));
                FilterData();
            }
        }

        public string ContactNumber
        {
            get => contactNumber;
            set
            {
                contactNumber = value;
                NotifyPropertyChanged(nameof(ContactNumber));
                FilterData();
            }
        }

        public string Email
        {
            get => email;
            set
            {
                email = value;
                NotifyPropertyChanged(nameof(Email));
                FilterData();
            }
        }

        public ObservableCollection<SkillDto> Skills { get; set; }

        public ObservableCollection<SkillDto> SelectedSkills
        {
            get => selectedSkills;
            set
            {
                selectedSkills = value;
                NotifyPropertyChanged(nameof(SelectedSkills));
                FilterData();
            }
        }
        

        public int AgeLowerLimit => employeeSettings.AgeLowerLimit;

        public int AgeHigherLimit => employeeSettings.AgeHigherLimit;

        #endregion

        #region Commands
        private ICommand skillSelectedCommand;

        public ICommand SkillSelectedCommand => skillSelectedCommand ?? (skillSelectedCommand = new RelayParameterizedCommand((o) =>
        {
            var skill = o as SkillDto;

            if (SelectedSkills.Contains(skill))
            {
                SelectedSkills.Remove(skill);
                FilterData();
                return;
            }
            SelectedSkills.Add(skill);
            FilterData();
        }));


        #endregion

        public FilterViewModel(EmployeeSettings employeeSettings, IQueryBus queryBus, IEventBus eventBus, ISkillService skillService)
        {
            this.queryBus = queryBus;
            this.eventBus = eventBus;
            this.employeeSettings = employeeSettings;
            this.skillService = skillService;
            name = string.Empty;
            lastName = string.Empty;
            contactNumber = string.Empty;
            email = string.Empty;
            minAge = employeeSettings.AgeLowerLimit;
            maxAge = employeeSettings.AgeHigherLimit;
            InitializeAsync();
            SelectedSkills = new ObservableCollection<SkillDto>();
        }
        private async void InitializeAsync()
        {
            Skills = new ObservableCollection<SkillDto>(await skillService.Browse());
        }

        private async void FilterData()
        {
            var query = new ParametrizedUserQuery
            {
                Email = Email,
                Name = Name,
                LastName = LastName,
                ContactNumber = ContactNumber,
                MinAge = MinAge,
                MaxAge = MaxAge,
                Skills = SelectedSkills
            };

            var filteredEmployees = await queryBus.Dispatch<ParametrizedUserQuery, IEnumerable<EmployeeDto>>(query);
            await eventBus.Dispatch(new EmployeeFilterChanged {FilteredEmployees = filteredEmployees});
        }

        public Task Handle(SkillAdded @event)
        {
            Skills.Add(@event.Skill);
            return Task.CompletedTask;
        }

        public Task Handle(SkillDeleted @event)
        {
            Skills.Remove(@event.Skill);
            return Task.CompletedTask;
        }
    }
}
