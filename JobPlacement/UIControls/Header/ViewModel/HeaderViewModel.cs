﻿using System.Threading.Tasks;
using System.Windows.Input;
using Infrastructure.DTO.Outgoing;
using Infrastructure.Events.User;
using Infrastructure.Handlers;
using JobPlacement.Employees.Views;

namespace JobPlacement.UIControls.Header.ViewModel
{
    public class HeaderViewModel : JobPlacement.ViewModel, IEventHandler<UserLoggedIn>
    {
        public UserDto LoggedInUser { get; private set; }

        #region Commands

        private ICommand addCommand;
        private ICommand editSkillsCommand;

        public ICommand AddCommand => addCommand ?? (addCommand = new RelayCommand(() =>
        {
            var addEmployeeWindow = new AddEmployee();
            addEmployeeWindow.Show();
        }));

        public ICommand EditSkillsCommand => editSkillsCommand ?? (editSkillsCommand = new RelayCommand(() =>
        {
            var editSkillsWindow = new EditSkills();
            editSkillsWindow.Show();
        }));
        #endregion

        public async Task Handle(UserLoggedIn @event)
        {
            LoggedInUser = @event.LoggedInUser;
            await Task.CompletedTask;
        }
    }
}
