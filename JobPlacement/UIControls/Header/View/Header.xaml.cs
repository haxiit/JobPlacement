﻿using System.Windows.Controls;
using JobPlacement.UIControls.Header.ViewModel;

namespace JobPlacement.UIControls.Header.View
{
    /// <summary>
    /// Interaction logic for Header.xaml
    /// </summary>
    public partial class Header : Page
    {
        public Header()
        {
            InitializeComponent();
            var viewModel = ViewModelResolver.Instance.ResolveViewModel<HeaderViewModel>();
            DataContext = viewModel;
        }
    }
}
