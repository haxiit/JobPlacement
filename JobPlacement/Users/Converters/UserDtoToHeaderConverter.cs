﻿using System;
using System.Globalization;
using System.Windows.Data;
using Infrastructure.DTO.Outgoing;

namespace JobPlacement.Users.Converters
{
    public class UserDtoToHeaderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is UserDto user)
            {
                return $"{user.Name} {user.LastName}";
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
