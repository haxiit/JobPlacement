﻿using System;
using System.Windows;
using JobPlacement.Users.ViewModels;

namespace JobPlacement.Users.Views
{
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            var viewModel = ViewModelResolver.Instance.ResolveViewModel<LoginWindowViewModel>();
            DataContext = viewModel;
            viewModel.CloseAction = Close;
            viewModel.ShowMainWindow = () => new MainWindow().Show();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var registerWinodw = new RegisterWindow();
            registerWinodw.Show();
            Close();
        }
    }
}