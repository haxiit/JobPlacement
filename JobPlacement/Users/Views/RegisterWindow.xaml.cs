﻿using System.Windows;
using System.Windows.Controls;
using JobPlacement.Users.ViewModels;

namespace JobPlacement.Users.Views
{
    /// <summary>
    /// Interaction logic for RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : Window 
    {
        public RegisterWindow()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            var viewModel = ViewModelResolver.Instance.ResolveViewModel<RegisterWindowViewModel>();
            DataContext = viewModel;
        }

        private void CancelButton_OnClick(object sender, RoutedEventArgs e)
        {
            var loginWindow = new LoginWindow();
            loginWindow.Show();
            this.Close();
        }
    }
}
