﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Infrastructure.Services.Users;

namespace JobPlacement.Users.ViewModels
{
    public class RegisterWindowViewModel : ViewModel
    {
        private readonly IUserService userService;
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmationPassword { get; set; }


        public RegisterWindowViewModel(IUserService userService)
        {
            this.userService = userService;
        }

        #region Commands

        private ICommand registerCommand;

        public ICommand RegisterCommand => registerCommand ?? (registerCommand = new RelayParameterizedCommand(async o =>
        {
            try
            {
                var passwords = (object[])o;
                char[] password = ((PasswordBox)passwords[0]).Password.ToCharArray();
                char[] confirmationPassword = ((PasswordBox)passwords[1]).Password.ToCharArray();
                if (!password.SequenceEqual(confirmationPassword))
                {
                    return;
                }
                await userService.Register(Email, Name, Lastname, password);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }));

        #endregion

    }
}
