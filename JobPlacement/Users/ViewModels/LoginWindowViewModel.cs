﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Infrastructure.Services.Users;

namespace JobPlacement.Users.ViewModels
{
    public class LoginWindowViewModel : ViewModel
    {
        private readonly IUserService userService;
        public string Email { get; set; }
        public string Password { get; set; }
        public Action CloseAction { get; set; }
        public Action ShowMainWindow { get; set; }

        public LoginWindowViewModel(IUserService userService)
        {
            this.userService = userService;
        }

        #region Commands 

        private ICommand loginCommand;

        public ICommand LoginCommand => loginCommand ?? (loginCommand = new RelayParameterizedCommand(async o =>
        {
            var passwordBox = o as PasswordBox;
            try
            {
                await userService.Login(Email, passwordBox.Password.ToCharArray());
                ShowMainWindow();
                CloseAction();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }));

        #endregion
    }
}
