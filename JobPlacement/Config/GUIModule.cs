﻿using System.Reflection;
using Autofac;
using Infrastructure.Handlers;
using Module = Autofac.Module;

namespace JobPlacement.Config
{
    public class GuiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(GuiModule).GetTypeInfo().Assembly;

            builder.RegisterAssemblyTypes(assembly)
                .Where(type => type.IsAssignableTo<ViewModel>())
                .AsSelf()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(assembly)
                .AsClosedTypesOf(typeof(IEventHandler<>))
                .InstancePerLifetimeScope();
        }
    }
}
