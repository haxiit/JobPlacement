﻿using Infrastructure.DTO.Outgoing;

namespace Infrastructure.Queries.Employees
{
    public class SearchForUnemployedEmployees : IQuery<EmployeeDto>
    {
        // Parameterless query
    }
}
