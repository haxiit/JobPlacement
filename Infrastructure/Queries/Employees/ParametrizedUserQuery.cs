﻿using System.Collections.Generic;
using Infrastructure.DTO.Outgoing;

namespace Infrastructure.Queries.Employees
{
    public class ParametrizedUserQuery : IQuery<IEnumerable<EmployeeDto>>, IQuery<IEnumerable<UserDto>>
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public IEnumerable<SkillDto> Skills { get; set; }
    }
}
