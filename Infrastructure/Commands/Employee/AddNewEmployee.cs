﻿namespace Infrastructure.Commands.Employee
{
    public class AddNewEmployee : ICommand
    {
        public string Name { get; set; }
        public string LastName { get; set; }
    }
}
