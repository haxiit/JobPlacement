﻿using Infrastructure.DTO.Outgoing;

namespace Infrastructure.Events.User
{
    public class UserLoggedIn : IEvent
    {
        public UserDto LoggedInUser { get; set; }
    }
}
