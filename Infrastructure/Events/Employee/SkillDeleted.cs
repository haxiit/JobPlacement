﻿using Infrastructure.DTO.Outgoing;

namespace Infrastructure.Events.Employee
{
    public class SkillDeleted : IEvent
    {
        public SkillDto Skill { get; set; }
    }
}
