﻿using Infrastructure.DTO.Outgoing;

namespace Infrastructure.Events.Employee
{
    public class EmployeeUpdated : IEvent
    {
        public EmployeeDto Employee { get; set; }
    }
}
