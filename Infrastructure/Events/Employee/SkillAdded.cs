﻿using Infrastructure.DTO.Outgoing;

namespace Infrastructure.Events.Employee
{
    public class SkillAdded : IEvent
    {
        public SkillDto Skill { get; set; }
    }
}
