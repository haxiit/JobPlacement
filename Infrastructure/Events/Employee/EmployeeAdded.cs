﻿using Infrastructure.DTO.Outgoing;

namespace Infrastructure.Events.Employee
{
    public class EmployeeAdded : IEvent
    {
        public EmployeeDto Employee { get; set; }
    }
}
