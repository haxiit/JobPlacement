﻿using System;

namespace Infrastructure.Events.Employee
{
    public class EmployeeDeleted : IEvent
    {
        public string Email { get; set; }
    }
}
