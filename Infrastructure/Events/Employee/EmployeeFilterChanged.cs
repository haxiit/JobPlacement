﻿using System.Collections.Generic;
using Infrastructure.DTO.Outgoing;

namespace Infrastructure.Events.Employee
{
    public class EmployeeFilterChanged : IEvent
    {
        public IEnumerable<EmployeeDto> FilteredEmployees { get; set; }
    }
}
