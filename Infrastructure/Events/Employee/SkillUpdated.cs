﻿using Infrastructure.DTO.Outgoing;

namespace Infrastructure.Events.Employee
{
    public class SkillUpdated : IEvent
    {
        public SkillDto Skill { get; set; }
    }
}
