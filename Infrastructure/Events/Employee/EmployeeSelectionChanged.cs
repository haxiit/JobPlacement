﻿using Infrastructure.DTO.Outgoing;

namespace Infrastructure.Events.Employee
{
    public class EmployeeSelectionChanged : IEvent
    {
        public EmployeeDto Employee { get; set; }
    }
}
