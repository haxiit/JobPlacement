﻿using Microsoft.Extensions.Configuration;

namespace Infrastructure.Extensions
{
    public static class SettingExtensions
    {
        public static T GetSettings<T>(this IConfiguration configuration) where T : new()
        {
            var section = typeof(T).Name.Replace("Settings", string.Empty);
            var configurationSection = configuration.GetSection(section);
            var settings = new T();

            configurationSection.Bind(settings);
            return settings;
        }
    }
}
