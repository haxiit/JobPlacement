﻿using System;

namespace Infrastructure.Extensions
{
    public static class StringExtensions
    {
        public static byte[] GetBytes(this string value)
        {
            var bytes = new byte[value.Length * sizeof(char)];
            Buffer.BlockCopy(value.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static bool StartsWithIgnoreCase(this string compared, string comparing)
            => compared.ToLowerInvariant().StartsWith(comparing.ToLowerInvariant());
    }
}
