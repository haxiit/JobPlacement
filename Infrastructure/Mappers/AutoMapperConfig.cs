﻿using AutoMapper;
using Core.Company.Entities;
using Core.Contract.Entities;
using Core.Employees.Entities;
using Core.Shared;
using Core.Users.Entities;
using Infrastructure.DTO.Incoming;
using Infrastructure.DTO.Outgoing;

namespace Infrastructure.Mappers
{
    public static class AutoMapperConfig
    {
        public static IMapper Initialize()
        {
           return new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Employee, EmployeeDto>();
                cfg.CreateMap<Employee, EmployeeDescriptor>();
                cfg.CreateMap<EmployeeDescriptor, Employee>();
                cfg.CreateMap<Company, CompanyDto>();
                cfg.CreateMap<Address, AddressDto>();
                cfg.CreateMap<Skill, SkillDto>();
                cfg.CreateMap<SkillDto, Skill>();
                cfg.CreateMap<AddressDescriptor, Address>();
                cfg.CreateMap<Contract, ContractDto>();
                cfg.CreateMap<User, UserDto>();
            }).CreateMapper();
        }
    }
}
