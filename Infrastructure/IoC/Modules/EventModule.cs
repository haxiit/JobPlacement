﻿using System.Reflection;
using Autofac;
using Infrastructure.Handlers;
using Module = Autofac.Module;

namespace Infrastructure.IoC.Modules
{
    public class EventModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(EventModule).GetTypeInfo().Assembly;

            builder.RegisterAssemblyTypes(assembly)
                .AsClosedTypesOf(typeof(IEventHandler<>))
                .InstancePerLifetimeScope();

            builder.RegisterType<EventBus>()
                .As<IEventBus>()
                .InstancePerLifetimeScope();
        }
    }
}
