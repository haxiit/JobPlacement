﻿using Autofac;
using Infrastructure.Repositories.Employees.Sql;
using Infrastructure.Repositories.Users.Sql;

namespace Infrastructure.IoC.Modules
{
    public class SqlModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<JobPlacementContext>()
                .AsSelf()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserContext>()
                .AsSelf()
                .InstancePerLifetimeScope();
        }
    }
}
