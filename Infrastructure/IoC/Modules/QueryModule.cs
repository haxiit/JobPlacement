﻿using System.Collections.Generic;
using Autofac;
using Infrastructure.DTO.Outgoing;
using Infrastructure.Handlers;
using Infrastructure.Handlers.Queries;
using Infrastructure.Queries.Employees;
using Module = Autofac.Module;

namespace Infrastructure.IoC.Modules
{
    public class QueryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ParametrizedUserQueryHandler>()
                .As<IQueryHandler<ParametrizedUserQuery, IEnumerable<EmployeeDto>>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<QueryBus>()
                .As<IQueryBus>()
                .InstancePerLifetimeScope();
        }
    }
}
