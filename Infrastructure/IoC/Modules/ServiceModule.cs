﻿using System.Reflection;
using Autofac;
using Infrastructure.Services;
using Infrastructure.Services.Authentication;
using Infrastructure.Services.Employees;
using Module = Autofac.Module;

namespace Infrastructure.IoC.Modules
{
    public class ServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(ServiceModule).GetTypeInfo().Assembly;

            builder.RegisterAssemblyTypes(assembly)
                .Where(type => type.IsAssignableTo<IService>())
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<Encrypter>().As<IEncrypter>().SingleInstance();

            builder.RegisterType<FtpStorageCvManager>()
                .As<ICvStorageManager>()
                .InstancePerLifetimeScope();
        }
    }
}
