﻿using Autofac;
using Infrastructure.Configuration;
using Infrastructure.Extensions;
using Microsoft.Extensions.Configuration;

namespace Infrastructure.IoC.Modules
{
    public class SettingsModule : Module
    {
        private readonly IConfiguration configuration;

        public SettingsModule(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(configuration.GetSettings<PersistanceSettings>()).SingleInstance();
            builder.RegisterInstance(configuration.GetSettings<EmployeeSettings>()).SingleInstance();
            builder.RegisterInstance(configuration.GetSettings<FileStorageSettings>()).SingleInstance();
        }
    }
}
