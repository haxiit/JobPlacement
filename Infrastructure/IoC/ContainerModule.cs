﻿using Autofac;
using Infrastructure.IoC.Modules;
using Infrastructure.Mappers;

namespace Infrastructure.IoC
{
    public class ContainerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(AutoMapperConfig.Initialize()).SingleInstance();
            builder.RegisterModule<RepositoryModule>();
            builder.RegisterModule<ServiceModule>();
            builder.RegisterModule<CommandModule>();
            builder.RegisterModule<QueryModule>();
            builder.RegisterModule<SqlModule>();
            builder.RegisterModule<EventModule>();
        }
    }
}
