﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Employees.Repositories;
using Core.Employees.Entities;

namespace Infrastructure.Repositories.Employees
{
    public class InMemoryEmployeeRepository : IEmployeeRepository
    {
        private readonly List<Employee> employees = new List<Employee>(); 
        public async Task Add(Employee employee)
        {
            employees.Add(employee);
            await Task.CompletedTask;
        }

        public async Task Update(Employee employee)
        {
            throw new NotImplementedException();
        }

        public async Task<Employee> Get(Guid id)
        {
            return await Task.FromResult(employees.FirstOrDefault(employee => employee.Id == id));
        }

        public async Task<Employee> GetByEmail(string email)
        {
            return await Task.FromResult(employees.FirstOrDefault(employee => employee.Email == email));
        }

        public async Task<List<Employee>> Browse()
        {
            return await Task.FromResult(employees);
        }

        public async Task Delete(Guid id)
        {
            var employee = await Get(id);
            if (employee != null) employees.Remove(employee);
        }
        public async Task DeleteByEmail(string email)
        {
            var employee = await GetByEmail(email);
            if (employee != null) employees.Remove(employee);
        }
    }
}
