﻿using System;
using Core.Employees.Entities;
using Infrastructure.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.Employees.Sql
{
    public class JobPlacementContext : DbContext
    {
        private readonly PersistanceSettings _persistanceSettings;
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Skill> Skills { get; set; }

        public JobPlacementContext(PersistanceSettings persistanceSettings)
        {
            _persistanceSettings = persistanceSettings;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (_persistanceSettings.InMemory)
            {
                optionsBuilder.UseInMemoryDatabase("InMemoryDb");
                return;
            }
            optionsBuilder.UseNpgsql(_persistanceSettings.ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var employeeEntity = modelBuilder.Entity<Employee>();
            employeeEntity.HasKey(employee => employee.Id);
            employeeEntity.Property(employee => employee.Id).HasColumnName("id");
            employeeEntity.Property(employee => employee.Name).HasColumnName("name");
            employeeEntity.Property(employee => employee.LastName).HasColumnName("last_name");
            employeeEntity.Property(employee => employee.BirthDate).HasColumnName("birth_date");
            employeeEntity.Property(employee => employee.SerialIdNumber).HasColumnName("serial_id_number");
            employeeEntity.Property(employee => employee.Pesel).HasColumnName("pesel");
            employeeEntity.Property(employee => employee.Email).HasColumnName("email");
            employeeEntity.Property(employee => employee.ContactNumber).HasColumnName("contact_number");
            employeeEntity.Property(employee => employee.CvUri).HasColumnName("cv_uri");
            employeeEntity.Ignore(employee => employee.CompanyId);
            employeeEntity.Ignore(employee => employee.ContractId);
            employeeEntity.Ignore(employee => employee.Skills);
            employeeEntity.ToTable("employees");

            var addressEntity = employeeEntity.OwnsOne(employee => employee.Address);
            addressEntity.Property(address => address.Street).HasColumnName("street");
            addressEntity.Property(address => address.PostalCode).HasColumnName("postal_code");
            addressEntity.Property(address => address.City).HasColumnName("city");

            var skillEntity = modelBuilder.Entity<Skill>();
            skillEntity.HasKey(skill => skill.Id);
            skillEntity.Property(skill => skill.Id).HasColumnName("id");
            skillEntity.Property(skill => skill.Name).HasColumnName("name");
            skillEntity.ToTable("skills");

            var employeeSkillEntity = modelBuilder.Entity<EmployeeSkill>();
            employeeSkillEntity.HasKey(employeeSkill => new { employeeSkill.EmployeeId, employeeSkill.SkillId });
            employeeSkillEntity.Property(employeeSkill => employeeSkill.EmployeeId).HasColumnName("employee_id");
            employeeSkillEntity.Property(employeeSkill => employeeSkill.SkillId).HasColumnName("skill_id");

            employeeSkillEntity
                .HasOne(employeeSkill => employeeSkill.Employee)
                .WithMany(employee => employee.EmployeeSkills)
                .HasForeignKey(employeeSkill => employeeSkill.EmployeeId)
                .HasConstraintName("employee_fk");

            employeeSkillEntity
                .HasOne(employeeSkill => employeeSkill.Skill)
                .WithMany(skill => skill.EmployeeSkills)
                .HasForeignKey(employeeSkill => employeeSkill.SkillId)
                .HasConstraintName("skill_fk");

            employeeSkillEntity.ToTable("employee_skills");
        }
    }
}
