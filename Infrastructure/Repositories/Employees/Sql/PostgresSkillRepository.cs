﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Employees.Entities;
using Core.Employees.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.Employees.Sql
{
    public class PostgresSkillRepository : ISkillRepository
    {
        private readonly JobPlacementContext skillContext;

        public PostgresSkillRepository(JobPlacementContext skillContext)
        {
            this.skillContext = skillContext;
        }

        public async Task Add(Skill skill)
        {
            await skillContext.AddAsync(skill);
            await skillContext.SaveChangesAsync();
        }

        public async Task AddMany(IEnumerable<Skill> skills)
        {
            await skillContext.AddRangeAsync(skills);
            await skillContext.SaveChangesAsync();
        }

        public async Task<Skill> Get(Guid skillId)
        {
            return await skillContext.Skills.SingleOrDefaultAsync(skill => skill.Id == skillId);
        }

        public async Task<Skill> GetByName(string skillName)
        {
            return await skillContext.Skills.SingleOrDefaultAsync(skill => skill.Name == skillName);
        }

        public async Task<IEnumerable<Skill>> Browse()
        {
            return await skillContext.Skills.ToListAsync();
        }

        public async Task Update(Skill skill)
        {
            var skillToUpdate = await Get(skill.Id);
            skillContext.Update(skillToUpdate);
            await skillContext.SaveChangesAsync();
        }

        public async Task Delete(Guid skillId)
        {
            var skill = await Get(skillId);
            skillContext.Remove(skill);
            await skillContext.SaveChangesAsync();
        }
    }
}
