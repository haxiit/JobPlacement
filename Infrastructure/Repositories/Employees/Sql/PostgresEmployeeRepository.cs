﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Employees.Entities;
using Core.Employees.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.Employees.Sql
{
    public class PostgresEmployeeRepository : ISqlRepository, IEmployeeRepository
    {
        private readonly JobPlacementContext employeeContext;

        public PostgresEmployeeRepository(JobPlacementContext employeeContext)
        {
            this.employeeContext = employeeContext;
        }

        public async Task Add(Employee employee)
        {
            await employeeContext.Employees.AddAsync(employee);
            await employeeContext.SaveChangesAsync();
        }

        public async Task Update(Employee employee)
        {
            var entity = await Get(employee.Id);
            employeeContext.Update(entity);
            await employeeContext.SaveChangesAsync();
        }

        public async Task<Employee> Get(Guid id)
        {
            return await employeeContext.Employees.SingleOrDefaultAsync((emoployee) => emoployee.Id == id);
        }

        public async Task<Employee> GetByEmail(string email)
        {
            return await employeeContext.Employees
                .Include(employee => employee.EmployeeSkills)
                .ThenInclude(employee => employee.Skill)
                .SingleOrDefaultAsync(emoployee => emoployee.Email.Equals(email));
        }

        public async Task<List<Employee>> Browse()
        {
            return await employeeContext.Employees
                .Include(employee => employee.EmployeeSkills)
                .ThenInclude(employee => employee.Skill)
                .ToListAsync();
        }

        public async Task Delete(Guid id)
        {
            var employee = await Get(id);
            employeeContext.Employees.Remove(employee);
            await employeeContext.SaveChangesAsync();
        }

        public async Task DeleteByEmail(string email)
        {
            var employee = await GetByEmail(email);
            employeeContext.Employees.Remove(employee);
            await employeeContext.SaveChangesAsync();
        }
    }
}
