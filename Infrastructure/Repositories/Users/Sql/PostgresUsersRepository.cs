﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Users.Entities;
using Core.Users.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.Users.Sql
{
    public class PostgresUsersRepository : ISqlRepository, IUserRepository
    {
        private readonly UserContext userContext;

        public PostgresUsersRepository(UserContext userContext)
        {
            this.userContext = userContext;
        }

        public async Task Add(User user)
        {
            await userContext.Users.AddAsync(user);
            await userContext.SaveChangesAsync();
        }

        public async Task Update(User user)
        {
            var entity = await Get(user.Id);
            userContext.Update(entity);
            await userContext.SaveChangesAsync();
        }

        public async Task<User> Get(Guid id)
        {
            return await userContext.Users.SingleOrDefaultAsync((user) => user.Id == id);
        }

        public async Task<User> GetByEmail(string email)
        {
            return await userContext.Users.SingleOrDefaultAsync((user) => user.Email == email);
        }

        public async Task<IEnumerable<User>> Browse()
        {
            return await userContext.Users.ToListAsync();
        }

        public async Task Delete(Guid id)
        {
            var user = await Get(id);
            userContext.Users.Remove(user);
            await userContext.SaveChangesAsync();
        }
    }
}
