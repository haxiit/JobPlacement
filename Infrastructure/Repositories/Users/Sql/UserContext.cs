﻿using Core.Users.Entities;
using Infrastructure.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.Users.Sql
{
    public class UserContext : DbContext
    {
        private readonly PersistanceSettings _persistanceSettings;

        public DbSet<User> Users { get; set; }

        public UserContext(PersistanceSettings persistanceSettings)
        {
            _persistanceSettings = persistanceSettings;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (_persistanceSettings.InMemory)
            {
                optionsBuilder.UseInMemoryDatabase("InMemoryDb");
                return;
            }
            optionsBuilder.UseNpgsql(_persistanceSettings.ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var userEntity = modelBuilder.Entity<User>();
            userEntity.HasKey(user => user.Id);
            userEntity.Property(user => user.Id).HasColumnName("id");
            userEntity.Property(user => user.Email).HasColumnName("email");
            userEntity.Property(user => user.Name).HasColumnName("name");
            userEntity.Property(user => user.LastName).HasColumnName("last_name");
            userEntity.Property(user => user.Password).HasColumnName("password");
            userEntity.Property(user => user.Salt).HasColumnName("salt");
            userEntity.Property(user => user.Role).HasColumnName("role");
            userEntity.ToTable("users");
        }
    }
}
