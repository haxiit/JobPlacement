﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Company.Entities;
using Core.Company.Repositories;

namespace Infrastructure.Repositories.Companies
{
    class InMemoryCompanyRepository : ICompanyRepository
    {
        private readonly List<Company> companies = new List<Company>();
        public async Task Add(Company company)
        {
            companies.Add(company);
            await Task.CompletedTask;
        }

        public async Task Update(Company company)
        {
            throw new NotImplementedException();
        }

        public async Task<Company> Get(Guid id)
        {
            return await Task.FromResult(companies.FirstOrDefault(company => company.Id == id));
        }

        public async Task<List<Company>> Browse()
        {
            return await Task.FromResult(companies);
        }

        public async Task Delete(Guid id)
        {
            var company = await Get(id);
            if (company != null) companies.Remove(company);
        }
    }
}
