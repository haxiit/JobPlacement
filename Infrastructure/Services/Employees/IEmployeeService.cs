﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Infrastructure.DTO.Incoming;
using Infrastructure.DTO.Outgoing;

namespace Infrastructure.Services.Employees
{
    public interface IEmployeeService : IService
    {
        Task Add(EmployeeDescriptor employeeDescriptor);
        Task Update(EmployeeDescriptor employeeDescriptor);
        Task AttachCv(Guid employeeId, string filePath);
        Task<EmployeeDto> Get(Guid id);
        Task<IEnumerable<EmployeeDto>> Browse();
        Task DeleteById(Guid id);
        Task DeleteByEmail(string email);
        Task<EmployeeDescriptor> GetDescriptiorByEmail(string email);
        Task<EmployeeDto> GetByEmail(string email);
    }
}
