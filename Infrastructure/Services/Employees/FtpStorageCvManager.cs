﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Infrastructure.Configuration;

namespace Infrastructure.Services.Employees
{
    public class FtpStorageCvManager : ICvStorageManager
    {
        private readonly FileStorageSettings fileStorageSettings;

        public FtpStorageCvManager(FileStorageSettings fileStorageSettings)
        {
            this.fileStorageSettings = fileStorageSettings;
        }

        public async Task DownloadCv(string cvFileName)
        {
            if (string.IsNullOrWhiteSpace(cvFileName) || !Path.GetExtension(cvFileName).Equals(".pdf"))
            {
                throw new ArgumentException("File extension has not been provided or is invalid (only .pdf is supported)");
            }

            using (var webClient = new WebClient())
            {
                try
                {
                    var filename = Path.GetFileName(cvFileName);
                    webClient.Credentials = new NetworkCredential(fileStorageSettings.Login, fileStorageSettings.Password);
                    var appData = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                        "JobPlacement");
                    await webClient.DownloadFileTaskAsync(new Uri(fileStorageSettings.ServerAddress + filename), 
                        Path.Combine(appData, filename));
                }
                catch (Exception e)
                {
                    throw new Exception("Could not download CV from the server", e);
                }
            }
        }

        public async Task<string> UploadCv(string fileToUploadPath)
        {
            if (string.IsNullOrWhiteSpace(fileToUploadPath) || !Path.GetExtension(fileToUploadPath).Equals(".pdf"))
            {
                throw new ArgumentException("File extension has not been provided or is invalid (only .pdf is supported)");
            }

            using (var webClient = new WebClient())
            {
                try
                {
                    webClient.Credentials = new NetworkCredential(fileStorageSettings.Login, fileStorageSettings.Password);
                    var uniqueFilename = $"{Path.GetFileNameWithoutExtension(fileToUploadPath)}-{Guid.NewGuid()}.pdf";
                    var uri = new Uri($"{fileStorageSettings.ServerAddress}{uniqueFilename}");
                    await webClient.UploadFileTaskAsync(uri, WebRequestMethods.Ftp.UploadFile, fileToUploadPath);
                    return uri.AbsoluteUri;
                }
                catch (Exception e)
                {
                    throw new Exception("Could not upload CV to the server", e);
                }
            }
        }

        public async Task DeleteCv(string path)
        {
            if (string.IsNullOrWhiteSpace(path) || !Path.GetExtension(path).Equals(".pdf"))
            {
                throw new ArgumentException("File extension has not been provided or is invalid (only .pdf is supported)");
            }

            try
            {
                var deleteRequest = (FtpWebRequest) WebRequest.Create(path);
                var credentials = new NetworkCredential(fileStorageSettings.Login, fileStorageSettings.Password);
                deleteRequest.Method = WebRequestMethods.Ftp.DeleteFile;
                deleteRequest.Credentials = credentials;

                var response = (FtpWebResponse) await deleteRequest.GetResponseAsync();
                response.Close();
            }
            catch (Exception e)
            {
                throw new Exception("Could not delete CV to the server", e);
            }
        }
    }
}
