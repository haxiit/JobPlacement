﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Infrastructure.DTO.Outgoing;

namespace Infrastructure.Services.Employees
{
    public interface ISkillService : IService
    {
        Task Add(SkillDto skill);
        Task Delete(SkillDto skill);
        Task<IEnumerable<SkillDto>> Browse();
    }
}
