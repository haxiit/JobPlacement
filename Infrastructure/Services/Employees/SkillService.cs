﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Core.Employees.Entities;
using Core.Employees.Repositories;
using Infrastructure.DTO.Outgoing;
using Infrastructure.Events.Employee;
using Infrastructure.Handlers;

namespace Infrastructure.Services.Employees
{
    public class SkillService : ISkillService
    {
        private readonly ISkillRepository repository;
        private readonly IMapper mapper;
        private readonly IEventBus eventBus;

        public SkillService(IEventBus eventBus, IMapper mapper, ISkillRepository repository)
        {
            this.eventBus = eventBus;
            this.mapper = mapper;
            this.repository = repository;
        }

        public async Task Add(SkillDto skill)
        {
            await repository.Add(mapper.Map<SkillDto, Skill>(skill));
            await eventBus.Dispatch(new SkillAdded { Skill = skill });
        }

        public async Task Delete(SkillDto skill)
        {
            var Skill = await repository.GetByName(skill.Name);
            await repository.Delete(Skill.Id);
            await eventBus.Dispatch(new SkillDeleted { Skill = skill });
        }

        public async Task<IEnumerable<SkillDto>> Browse()
        {
            var skills = await repository.Browse();
            return mapper.Map<IEnumerable<Skill>, IEnumerable<SkillDto>>(skills);
        }
    }
}
