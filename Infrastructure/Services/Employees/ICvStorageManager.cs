﻿using System.Threading.Tasks;

namespace Infrastructure.Services.Employees
{
    public interface ICvStorageManager
    {
        Task DownloadCv(string path);
        Task<string> UploadCv(string sourcePath);
        Task DeleteCv(string path);
    }
}
