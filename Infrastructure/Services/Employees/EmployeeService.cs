﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Core.Employees.Entities;
using Core.Employees.Repositories;
using Core.Shared;
using Infrastructure.DTO.Incoming;
using Infrastructure.DTO.Outgoing;
using Infrastructure.Events.Employee;
using Infrastructure.Handlers;

namespace Infrastructure.Services.Employees
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository repository;
        private readonly IMapper mapper;
        private readonly IEventBus eventBus;
        private readonly ICvStorageManager cvStorageManager;

        public EmployeeService(IEmployeeRepository repository, IMapper mapper, IEventBus eventBus, ICvStorageManager cvStorageManager)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.eventBus = eventBus;
            this.cvStorageManager = cvStorageManager;
        }

        public async Task Add(EmployeeDescriptor employeeDescriptor)
        {
            var employee = Employee.Create(employeeDescriptor.Name, 
                employeeDescriptor.LastName,
                employeeDescriptor.Birthdate, 
                employeeDescriptor.SerialIdNumber, 
                employeeDescriptor.Pesel,
                employeeDescriptor.Email, employeeDescriptor.ContactNumber, 
                new Address(employeeDescriptor.Address.Street, 
                            employeeDescriptor.Address.PostalCode, 
                            employeeDescriptor.Address.City), 
                employeeDescriptor.Skills);

            await repository.Add(employee);

            if (!string.IsNullOrWhiteSpace(employeeDescriptor.CvUri))
            {
                await AttachCv(employee.Id, employeeDescriptor.CvUri);
            }

            var employeeDto = mapper.Map<Employee, EmployeeDto>(employee);
            await eventBus.Dispatch(new EmployeeAdded {Employee = employeeDto});
        }

        public async Task Update(EmployeeDescriptor employeeDescriptor)
        {
            var employee = await repository.GetByEmail(employeeDescriptor.Email);
            var address = new Address(employeeDescriptor.Address.Street, employeeDescriptor.Address.PostalCode, employeeDescriptor.Address.City);
            var skills = employeeDescriptor.Skills;

            employee.ChangeName(employeeDescriptor.Name);
            employee.ChangeLastName(employeeDescriptor.LastName);
            employee.ChangeAddress(address);
            employee.ChangeNumber(employeeDescriptor.ContactNumber);
            employee.ChangeEmail(employeeDescriptor.Email);
            employee.ChangeSerialIdNumber(employeeDescriptor.SerialIdNumber);
            employee.ChangePesel(employeeDescriptor.Pesel);
            employee.ChangeSkills(skills);

            await repository.Update(employee);
            var employeeDto = mapper.Map<Employee, EmployeeDto>(employee);
            await eventBus.Dispatch(new EmployeeUpdated { Employee = employeeDto });
        }

        public async Task AttachCv(Guid employeeId, string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new ArgumentException($"Given file on {filePath} path doesn't exist");
            }

            var employee = await repository.Get(employeeId);
            if (employee == null)
            {
                throw new ArgumentException($"Employee with ID {employeeId} doesn't exist");
            }

            var createdFilename = await cvStorageManager.UploadCv(filePath);

            if (!string.IsNullOrWhiteSpace(employee.CvUri))
            {
                // Delete from remote
                await cvStorageManager.DeleteCv(employee.CvUri);

                // Delete from local
                var currentCvFilename = Path.GetFileName(employee.CvUri);
                var localCvPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    "JobPlacement",
                    currentCvFilename);

                if(File.Exists(localCvPath))
                    File.Delete(localCvPath);
            }
            employee.AttachCv(createdFilename);
            await repository.Update(employee);
        }

        public async Task<EmployeeDto> Get(Guid id)
        {
            var employee = await repository.Get(id);
            return mapper.Map<Employee, EmployeeDto>(employee);
        }
        public async Task<EmployeeDescriptor> GetDescriptiorByEmail(string email)
        {
            var employee = await repository.GetByEmail(email);
            return mapper.Map<Employee, EmployeeDescriptor>(employee);
        }

        public async Task<EmployeeDto> GetByEmail(string email)
        {
            var employee = await repository.GetByEmail(email);
            return mapper.Map<Employee, EmployeeDto>(employee);
        }

        public async Task<IEnumerable<EmployeeDto>> Browse()
        {
            var employees = await repository.Browse();
            return mapper.Map<IEnumerable<Employee>, IEnumerable<EmployeeDto>>(employees);
        }

        public async Task DeleteById(Guid id)
        {
            await repository.Delete(id);
        }

        public async Task DeleteByEmail(string email)
        {
            var employee = await repository.GetByEmail(email);
            if (!string.IsNullOrWhiteSpace(employee.CvUri))
            {
                try
                {
                    await cvStorageManager.DeleteCv(employee.CvUri);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
            await repository.DeleteByEmail(email);
            await eventBus.Dispatch(new EmployeeDeleted {Email = email});
        }
    }
}
