﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Core.Company.Entities;
using Core.Company.Repositories;
using Core.Shared;
using Infrastructure.DTO;
using Infrastructure.DTO.Outgoing;

namespace Infrastructure.Services.Companies
{
    public class CompanyService : ICompanyService
    {
        private readonly ICompanyRepository repository;
        private readonly IMapper mapper;

        public CompanyService(ICompanyRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public async Task Add(string name, ISet<Address> locations, string identifier, string contactNumber, string email)
        {
            var company = new Company(name, locations, identifier, contactNumber, email);
            await repository.Add(company);
        }

        public async Task<CompanyDto> Get(Guid id)
        {
            var company = await repository.Get(id);
            return mapper.Map<Company, CompanyDto>(company);
        }

        public async Task<IEnumerable<CompanyDto>> Browse()
        {
            return mapper.Map<IEnumerable<Company>, IEnumerable<CompanyDto>>(await repository.Browse());
        }
    }
}
