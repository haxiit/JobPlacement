﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Shared;
using Infrastructure.DTO;
using Infrastructure.DTO.Outgoing;

namespace Infrastructure.Services.Companies
{
    public interface ICompanyService : IService
    {
        Task Add(string name, ISet<Address> locations, string identifier, string contactNumber, string email);

        Task<CompanyDto> Get(Guid id);

        Task<IEnumerable<CompanyDto>> Browse();
    }
}
