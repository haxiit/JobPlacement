﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Core.Company.Entities;
using Core.Company.Repositories;
using Core.Employees.Entities;
using Core.Employees.Repositories;
using Core.Shared;
using Core.Users.Entities;
using Core.Users.Repositories;
using Infrastructure.DTO.Outgoing;
using Infrastructure.Events.Employee;
using Infrastructure.Handlers;

namespace Infrastructure.Services.Utils
{
    public class DataSeeder : IDataSeeder
    {
        private readonly IEmployeeRepository employeeRepository;
        private readonly ICompanyRepository companyRepository;
        private readonly IUserRepository userRepository;
        private readonly ISkillRepository skillRepository;
        private readonly IEventBus eventBus;
        private readonly IMapper mapper;

        public bool Initialized { get; private set; }

        public DataSeeder(IEmployeeRepository employeeRepository, ICompanyRepository companyRepository, 
            IUserRepository userRepository, ISkillRepository skillRepository, IEventBus eventBus, IMapper mapper)
        {
            this.employeeRepository = employeeRepository;
            this.companyRepository = companyRepository;
            this.userRepository = userRepository;
            this.skillRepository = skillRepository;
            this.eventBus = eventBus;
            this.mapper = mapper;
        }

        public async Task SeedData()
        {
            if (Initialized) return;

            var user1 = User.Create("user@gmail.com", "Anal", "Walkoniak", 
                "Aua2r8tPPrY9pY4fb1gSUF7BN7LL8y5DTtgf2/Vp", "ArbzNo/no3Mxn72BaORWjrMRJ4iKl88TOi5TOKZn");
            await userRepository.Add(user1);

            var skills = new List<Skill>
            {
                new Skill("walenie gruchy"),
                new Skill("binding kurwa"),
                new Skill("potęga potu"),
                new Skill("krzyk mocy"),
                new Skill("stolarstwo"),
                new Skill("malarstwo"),
                new Skill("tynkarstwo"),
                new Skill("programowanie"),
                new Skill("gotowanie")
            };
            await skillRepository.AddMany(skills);

            var employee1 = Employee.Create("Adam", "Kucki", new DateTime(1995, 12, 20),
                "AYT293", "1239818123", "adam.kucki@korwin.pl", "549213653", new Address("Adamska", "71-949", "Szczecin"),
                new List<Skill> { skills[0], skills[1] }, @"ftp://ftp.agnieszkakb.nazwa.pl/CV/Michal Zurawski CV-52a2ff24-601d-41ee-a190-be5a1ced6081.pdf");
            var employee2 = Employee.Create("Sebastian", "Dawid", new DateTime(1994, 8, 16),
                "AYT293", "1239818124", "seba55@gmail.com", "243572322", new Address("Sebixa", "76-959", "Szczecin"), new List<Skill> { skills[2] },
                @"ftp://ftp.agnieszkakb.nazwa.pl/CV/wzmacniaczop-9a29ae1b-0ec5-4cf6-a78c-ee00244348e8.pdf");
            var employee3 = Employee.Create("Mariusz", "Manolo", new DateTime(1993, 1, 27),
                "AYT293", "1239818125", "marooo78@interia.pl", "666432555", new Address("Marioosza", "55-933", "Ustka"), new List<Skill> { skills[3], skills[4], skills[5], skills[6], skills[7], skills[8] });

            await employeeRepository.Add(employee1);
            await employeeRepository.Add(employee2);
            await employeeRepository.Add(employee3);

            var address = new HashSet<Address> { new Address("Beniowskiego 5", "70-230", "Szczecin") };

            var company1 = new Company("avid", address, "12312-123-231", "93249123", "avid@chuj.ka");
            var company2 = new Company("b1", address, "2999-123-231", "91119123", "b1@chuj.ka");

            await companyRepository.Add(company1);
            await companyRepository.Add(company2);

            Initialized = true;

            await Task.CompletedTask;
        }
    }
}
