﻿using System.Threading.Tasks;

namespace Infrastructure.Services.Utils
{
    public interface IDataSeeder : IService
    {
        Task SeedData();
    }
}
