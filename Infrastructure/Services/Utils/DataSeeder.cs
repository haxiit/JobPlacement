﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Company.Entities;
using Core.Company.Repositories;
using Core.Employees.Entities;
using Core.Employees.Repositories;
using Core.Shared;

namespace Infrastructure.Services.Utils
{
    public class EmployeeDataSeeder : IDataSeedService
    {
        private readonly IEmployeeRepository employeeRepository;
        private readonly ICompanyRepository companyRepository;


        public bool Initialized { get; private set; }

        public EmployeeDataSeeder(IEmployeeRepository employeeRepository, ICompanyRepository companyRepository)
        {
            this.employeeRepository = employeeRepository;
            this.companyRepository = companyRepository;
        }

        public async Task SeedData()
        {
            if (Initialized) return;

            var employee1 = Employee.Create("employee", "1", DateTime.Now,
                "AYT293", "1239818123", "kurwa@chuj.pl", "997", new Address("gunwo", "71-99", "Wegorzyno" ));
            var employee2 = Employee.Create("employee", "2", DateTime.Now,
                "AYT293", "1239818123", "gunwo@chuj.pl", "997", new Address("gunwo", "71-99", "Wegorzyno"));
            var employee3 = Employee.Create("employee", "3", DateTime.Now,
                "AYT293", "1239818123", "cipa@chuj.pl", "997", new Address("gunwo", "71-99", "Wegorzyno"));

            await employeeRepository.Add(employee1);
            await employeeRepository.Add(employee2);
            await employeeRepository.Add(employee3);

            var address = new HashSet<Address> { new Address("Beniowskiego 5", "70-230", "Szczecin") };

            var company1 = new Company("avid", address, "12312-123-231", "93249123", "avid@chuj.ka");
            var company2 = new Company("b1", address, "2999-123-231", "91119123", "b1@chuj.ka");

            await companyRepository.Add(company1);
            await companyRepository.Add(company2);
            Initialized = true;

            await Task.CompletedTask;
        }
    }
}
