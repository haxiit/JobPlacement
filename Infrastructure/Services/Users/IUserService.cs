﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Infrastructure.DTO.Outgoing;

namespace Infrastructure.Services.Users
{
    public interface IUserService : IService
    {
        Task Register(string email, string name, string lastName, char[] password);
        Task Login(string email, char[] password);
        Task<UserDto> Get(Guid id);
        Task<IEnumerable<UserDto>> Browse();
    }
}
