﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Core.Users.Entities;
using Core.Users.Repositories;
using Infrastructure.DTO.Outgoing;
using Infrastructure.Events.User;
using Infrastructure.Handlers;
using Infrastructure.Services.Authentication;

namespace Infrastructure.Services.Users
{
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;
        private readonly IMapper mapper;
        private readonly IEncrypter encrypter;
        private readonly IEventBus eventBus;

        public UserService(IUserRepository userRepository, IMapper mapper, IEncrypter encrypter, IEventBus eventBus)
        {
            this.userRepository = userRepository;
            this.mapper = mapper;
            this.encrypter = encrypter;
            this.eventBus = eventBus;
        }
   
        public async Task Register(string email, string name, string lastName, char[] password)
        {
            var entity = await userRepository.GetByEmail(email);
            if (entity != null)
            {
                throw new InvalidOperationException("User already exists");
            }
            var salt = encrypter.GetSalt(password);
            var hash = encrypter.GetHash(salt, password);
            var user = User.Create(email, name, lastName, hash, salt);
            await userRepository.Add(user);
        }

        public async Task Login(string email, char[] password)
        {
            var entity = await userRepository.GetByEmail(email);
            if (entity == null)
            {
                throw new InvalidOperationException("Invalid email or password");
            }

            var hash = encrypter.GetHash(entity.Salt, password);
            if (!entity.Password.Equals(hash))
            {
                throw new InvalidOperationException("Invalid email or password");
            }

            await eventBus.Dispatch(new UserLoggedIn { LoggedInUser = mapper.Map<User, UserDto>(entity)} );
        }

        public async Task<UserDto> Get(Guid id)
        {
            var user = await userRepository.Get(id);
            return mapper.Map<User, UserDto>(user);
        }

        public async Task<IEnumerable<UserDto>> Browse()
        {
            var users = await userRepository.Browse();
            return mapper.Map<IEnumerable<User>, IEnumerable<UserDto>>(users);
        }
    }
}
