﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Core.Contract.Entities;
using Core.Contract.Repositories;
using Core.Shared;
using Infrastructure.DTO;
using Infrastructure.DTO.Incoming;
using Infrastructure.DTO.Outgoing;

namespace Infrastructure.Services.Contracts
{
    public class ContractService : IContractService
    {
        private readonly IContractRepository repository;
        private readonly IMapper mapper;

        public ContractService(IContractRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }
        public async Task Add(DateTime startDate, DateTime dueDate, AddressDescriptor location)
        {
            var address = mapper.Map<AddressDescriptor, Address>(location);
            var contract = new Contract(startDate, dueDate, address);
            await repository.Add(contract);
        }

        public async Task<ContractDto> Get(Guid id)
        {
            var contract = await repository.Get(id);
            return mapper.Map<Contract, ContractDto>(contract);
        }

        public async Task<IEnumerable<ContractDto>> Browse()
        {
            var contracts = await repository.Browse();
            return mapper.Map<IEnumerable<Contract>, IEnumerable<ContractDto>>(contracts);
        }
    }
}
