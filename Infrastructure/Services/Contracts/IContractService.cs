﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Infrastructure.DTO.Incoming;
using Infrastructure.DTO.Outgoing;

namespace Infrastructure.Services.Contracts
{
    public interface IContractService : IService
    {
        Task Add(DateTime startDate, DateTime dueDate, AddressDescriptor location);

        Task<ContractDto> Get(Guid id);

        Task<IEnumerable<ContractDto>> Browse();
    }
}
