﻿using System;
using System.Security.Cryptography;
using Infrastructure.Extensions;

namespace Infrastructure.Services.Authentication
{
    public class Encrypter : IEncrypter
    {
        private const int HashingIterations = 10000;
        private const int SaltSize = 30;

        public string GetSalt(char[] password)
        {
            if (password.Length == 0)
            {
                throw new ArgumentException("Can not generate salt from an empty value.", nameof(password));
            }

            var saltBytes = new byte[SaltSize];
            var randomNumberGenerator = RandomNumberGenerator.Create();
            randomNumberGenerator.GetBytes(saltBytes);

            return Convert.ToBase64String(saltBytes);
        }

        public string GetHash(string salt, char[] password)
        {
            if (password.Length == 0)
            {
                throw new ArgumentException("Can not generate hash from an empty value.", nameof(password));
            }
            if (salt.Length == 0)
            {
                throw new ArgumentException("Can not use an empty salt from hashing value.", nameof(salt));
            }

            var pbkdf2 = new Rfc2898DeriveBytes(new string(password), salt.GetBytes(), HashingIterations);
            return Convert.ToBase64String(pbkdf2.GetBytes(SaltSize));
        }
    }
}
