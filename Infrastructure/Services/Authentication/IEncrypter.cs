﻿using System.Threading.Tasks;

namespace Infrastructure.Services.Authentication
{
    public interface IEncrypter
    {
        string GetSalt(char[] password);

        string GetHash(string salt, char[] password);
    }
}
