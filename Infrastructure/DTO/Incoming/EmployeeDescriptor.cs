﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Employees.Entities;
using Infrastructure.DTO.Outgoing;

namespace Infrastructure.DTO.Incoming
{
    public class EmployeeDescriptor
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public DateTime Birthdate { get; set; }
        public string SerialIdNumber { get; set; }
        public string Pesel { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public AddressDescriptor Address { get; set; }
        public IEnumerable<Skill> Skills { get; set; }
        public string CvUri { get; set; }
    }
}
