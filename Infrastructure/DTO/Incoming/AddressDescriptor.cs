﻿namespace Infrastructure.DTO.Incoming
{
    public class AddressDescriptor
    {
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
    }
}
