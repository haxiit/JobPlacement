﻿namespace Infrastructure.DTO.Outgoing
{
    public class SkillDto
    {
        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            SkillDto skill = obj as SkillDto;
            if (skill != null)
            {
                return skill.Name == Name;
            }

            return false;
        }
    }
}
