﻿using System;
using System.Collections.Generic;
using Core.Shared;

namespace Infrastructure.DTO.Outgoing
{
    public class CompanyDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ISet<Address> Locations { get; set; }
        public string Identifier { get; set; }
        public string ContactNumber { get; set; }  
        public string Email { get; set; }
    }
}
