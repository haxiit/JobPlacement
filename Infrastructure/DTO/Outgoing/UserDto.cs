﻿namespace Infrastructure.DTO.Outgoing
{
    public class UserDto
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
    }
}
