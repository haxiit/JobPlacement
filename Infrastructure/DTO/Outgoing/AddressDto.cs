﻿namespace Infrastructure.DTO.Outgoing
{
    public class AddressDto
    {
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
    }
}
