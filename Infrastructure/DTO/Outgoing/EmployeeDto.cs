﻿using System;
using System.Collections.Generic;

namespace Infrastructure.DTO.Outgoing
{
    public class EmployeeDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public IEnumerable<SkillDto> Skills { get; set; }
        public string CvUri { get; set; }
        //public Guid CompanyId { get; set; }

        public override string ToString()
        {
            return $"Name: {Name}";
        }
    }
}
