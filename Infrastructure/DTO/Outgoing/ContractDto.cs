﻿using System;
using Core.Shared;

namespace Infrastructure.DTO.Outgoing
{
    public class ContractDto
    {
        public DateTime StartDate { get; set; }
        public DateTime DueDate { get; set; }
        public Address Location { get; set; }
    }
}
