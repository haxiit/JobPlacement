﻿namespace Infrastructure.Configuration
{
    public class FileStorageSettings
    {
        public string ServerAddress { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
