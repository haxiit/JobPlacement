﻿namespace Infrastructure.Configuration
{
    public class PersistanceSettings
    {
        public bool InMemory { get; set; }
        public string ConnectionString { get; set; }
    }
}
