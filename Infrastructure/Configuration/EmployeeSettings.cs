﻿namespace Infrastructure.Configuration
{
    public class EmployeeSettings
    {
        public int AgeLowerLimit { get; set; }
        public int AgeHigherLimit { get; set; }
    }
}
