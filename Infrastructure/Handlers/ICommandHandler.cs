﻿using System.Threading.Tasks;
using Infrastructure.Commands;

namespace Infrastructure.Handlers
{
    public interface ICommandHandler<in T> where T : ICommand
    {
        Task Handle(T command);
    }
}
