﻿using System.Threading.Tasks;
using Infrastructure.Events;

namespace Infrastructure.Handlers
{
    public interface IEventBus
    {
        Task Dispatch<T>(T @event) where T : IEvent;
    }
}
