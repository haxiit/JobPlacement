﻿using System;
using System.Threading.Tasks;
using Infrastructure.Commands.Employee;
using Infrastructure.Services.Employees;

namespace Infrastructure.Handlers.Commands
{
    public class AddNewEmployeeHandler : ICommandHandler<AddNewEmployee>
    {
        private readonly IEmployeeService employeeService;

        public AddNewEmployeeHandler(IEmployeeService employeeService)
        {
            this.employeeService = employeeService;
        }

        public Task Handle(AddNewEmployee command)
        {
            throw new NotImplementedException();
        }
    }
}
