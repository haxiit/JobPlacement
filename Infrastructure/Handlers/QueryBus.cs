﻿using System;
using System.Threading.Tasks;
using Autofac;
using Infrastructure.Queries;

namespace Infrastructure.Handlers
{
    public class QueryBus : IQueryBus
    {
        private readonly IComponentContext _componentContext;

        public QueryBus(IComponentContext componentContext)
        {
            _componentContext = componentContext;
        }

        public async Task<R> Dispatch<T, R>(T query) where T : IQuery<R>
        {
            if (query == null)
            {
                throw new ArgumentNullException($"Query {nameof(query)} cannot be null");
            }

            var handler = _componentContext.Resolve<IQueryHandler<T, R>>();
            return await handler?.Handle(query);
        }
    }
}
