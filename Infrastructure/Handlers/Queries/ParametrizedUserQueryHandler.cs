﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Core.Employees.Entities;
using Core.Employees.Repositories;
using Infrastructure.DTO.Outgoing;
using Infrastructure.Extensions;
using Infrastructure.Queries.Employees;

namespace Infrastructure.Handlers.Queries
{
    public class ParametrizedUserQueryHandler : IQueryHandler<ParametrizedUserQuery, IEnumerable<EmployeeDto>>
    {
        private readonly IEmployeeRepository employeeRepository;
        private readonly IMapper mapper;

        public ParametrizedUserQueryHandler(IEmployeeRepository employeeRepository, IMapper mapper)
        {
            this.employeeRepository = employeeRepository;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<EmployeeDto>> Handle(ParametrizedUserQuery query)
        {
            var employees = await employeeRepository.Browse();
            var filteredEmployees = employees.Where(employee =>
            {
                var today = DateTime.Today;
                var currentAge = today.Year - employee.BirthDate.Year;

                // Go back to the year the person was born in case of a leap year
                if (employee.BirthDate > today.AddYears(-currentAge))
                    currentAge--;

                bool hasAllSkills = query.Skills.All(skill => employee.Skills.ToList().Contains(new Skill(skill.Name)));

                return employee.Email.StartsWithIgnoreCase(query.Email) &&
                       employee.Name.StartsWithIgnoreCase(query.Name) &&
                       employee.LastName.StartsWithIgnoreCase(query.LastName) &&
                       employee.ContactNumber.StartsWithIgnoreCase(query.ContactNumber) &&
                       currentAge >= query.MinAge && currentAge <= query.MaxAge &&
                       hasAllSkills;
            });
            return mapper.Map<IEnumerable<Employee>, IEnumerable<EmployeeDto>>(filteredEmployees);
        }
    }
}
