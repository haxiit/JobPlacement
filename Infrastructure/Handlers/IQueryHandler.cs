﻿using System.Threading.Tasks;
using Infrastructure.Queries;

namespace Infrastructure.Handlers
{
    public interface IQueryHandler<in T, R> where T : IQuery<R>
    {
        Task<R> Handle(T query);
    }
}
