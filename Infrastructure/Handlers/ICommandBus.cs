﻿using System.Threading.Tasks;
using Infrastructure.Commands;

namespace Infrastructure.Handlers
{
    public interface ICommandBus
    {
        Task Dispatch<T>(T command) where T : ICommand;
    }
}
