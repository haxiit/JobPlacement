﻿using System.Threading.Tasks;
using Infrastructure.Queries;

namespace Infrastructure.Handlers
{
    public interface IQueryBus
    {
        Task<R> Dispatch<T, R>(T query) where T : IQuery<R>;
    }
}
