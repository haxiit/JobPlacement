﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Autofac;
using Infrastructure.Events;

namespace Infrastructure.Handlers
{
    public class EventBus : IEventBus
    {
        private readonly IComponentContext componentContext;

        public EventBus(IComponentContext componentContext)
        {
            this.componentContext = componentContext;
        }

        public async Task Dispatch<T>(T @event) where T : IEvent
        {
            if (@event == null)
            {
                throw new ArgumentNullException($"Event {nameof(@event)} cannot be null");
            }

            var handlers = componentContext.Resolve<IEnumerable<IEventHandler<T>>>();

            foreach (var handler in handlers)
            {
                await handler.Handle(@event);
            }
        }
    }
}
