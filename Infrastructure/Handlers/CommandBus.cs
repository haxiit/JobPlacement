﻿using System;
using System.Threading.Tasks;
using Autofac;
using Infrastructure.Commands;

namespace Infrastructure.Handlers
{
    public class CommandBus : ICommandBus
    {
        private readonly IComponentContext context;

        public CommandBus(IComponentContext context)
        {
            this.context = context;
        }

        public async Task Dispatch<T>(T command) where T : ICommand
        {
            if (command == null)
            {
                throw new NullReferenceException("Command cannot be null reference");
            }

            var handler = context.Resolve<ICommandHandler<T>>();
            if (handler != null)
            {
               await handler.Handle(command);
            }
        }
    }
}
