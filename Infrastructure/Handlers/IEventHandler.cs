﻿using System.Threading.Tasks;
using Infrastructure.Events;

namespace Infrastructure.Handlers
{
    public interface IEventHandler<T> where T : IEvent
    {
        Task Handle(T @event);
    }
}
